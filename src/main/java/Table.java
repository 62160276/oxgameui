
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class Table implements Serializable{
    private char[][] table = {{'-','-','-'},
                     {'-','-','-'},
                     {'-','-','-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastRow,lastCol;
    private Player winner;
    private boolean finish = false;
    public Table(Player x,Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void showTable(){
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i+1+" ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    
    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    public boolean setRowCol(int row,int col){
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkWin();
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
            
    }
    public void checkCol(){
        for(int row=0;row<3;row++){
            if(table[row][lastCol]!= currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void setStatWinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }
    public void checkRow(){
        for(int col=0;col<3;col++){
            if(table[lastRow][col]!= currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    public void checkX(){
        if(table[0][0]==currentPlayer.getName()
                &&table[1][1]==currentPlayer.getName()
                &&table[2][2]==currentPlayer.getName()){
            winner = currentPlayer;
            finish = true;
            setStatWinLose();
        }
        else if(table[2][0]==currentPlayer.getName()
                &&table[1][1]==currentPlayer.getName()
                &&table[0][2]==currentPlayer.getName()){
            winner = currentPlayer;
            finish = true;
            setStatWinLose();
        }
         
    }
    public void checkDraw(){
        for(int row=0;row<table.length;row++){
            for(int col=0;col<table[row].length;col++){
                if(table[row][col]=='-'){
                    return;
                }
            }
        }
        finish = true;
        playerX.draw();
        playerO.draw();
    }
    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }
    public boolean isFinish(){
        return finish;  
    }
    public Player getWinner(){
        return winner;
    }
}
